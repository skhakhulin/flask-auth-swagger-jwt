from flask_jwt_extended import (
    create_access_token,
    jwt_required,
    get_raw_jwt,
    get_jwt_identity
)

from models import UserModel, RevokedTokenModel, ConfigModel


def fetch_accounts():
    return UserModel.return_all(), 200


def login(account):

    username = account.get("username", None)
    password = account.get("password", None)

    current_user = UserModel.find_by_username(username)

    if not current_user:
        return {'message': 'User {} doesn\'t exist'.format(username)}

    if UserModel.verify_hash(password, current_user.password):
        return {
            'access_token': create_access_token(identity=username)
        }, 200
    else:
        return 'Wrong credentials', 400


@jwt_required
def logout():
    jti = get_raw_jwt()['jti']

    revoked_token = RevokedTokenModel(jti=jti)
    revoked_token.add()

    return {'message': 'Access token has been revoked'}, 204



@jwt_required
def change_policy(**kwargs):

    user = UserModel.find_by_username(get_jwt_identity())

    if user.admin:
        return ConfigModel.change_policy(kwargs['password_policy'])
    else:
        return {'message': 'You don\'t have permission'}, 405


@jwt_required
def change_password(account_id, password):

    from_user = UserModel.find_by_username(get_jwt_identity())
    to_user = UserModel.find_by_id(account_id)

    if to_user:
        if from_user == to_user or from_user.admin:
            return UserModel.change_password(account_id, password['password'])
        else:
            return {'message': 'You don\'t have permission'}, 405
    else:
        return {'message': 'User with id {} does not exists'.format(account_id)}, 404


@jwt_required
def create_account(account):
    user = UserModel.find_by_username(get_jwt_identity())
    if user.admin:
        username = account.get("username", None)
        password = account.get("password", None)

        if UserModel.find_by_username(username):
            return {'message': 'User {} already exists'.format(username)}, 400

        UserModel(
            username=username,
            password=UserModel.generate_hash(password)
        ).save_to_db()

        return {
            'message': 'User {} created'.format(username)
        }, 201

    else:
        return {'message': 'You don\'t have permission'}, 405


@jwt_required
def delete_account(account_id):
    from_user = UserModel.find_by_username(get_jwt_identity())
    to_user = UserModel.find_by_id(account_id)

    if to_user:
        if from_user == to_user or from_user.admin:
            if from_user == to_user:
                logout()
            return UserModel.delete_by_id(account_id)
        else:
            return {'message': 'You don\'t have permission'}, 405
    else:
        return {'message': 'User with id {} does not exists'.format(account_id)}, 404
