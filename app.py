import connexion

from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
from flask_script import Manager


connexion_app = connexion.App(__name__, specification_dir='swagger/')

flask_app = connexion_app.app
flask_app.config.from_pyfile('settings/settings.cfg')

db = SQLAlchemy(flask_app)
jwt = JWTManager(flask_app)
manager = Manager(flask_app)

