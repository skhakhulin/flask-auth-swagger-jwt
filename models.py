import re
from app import db
from passlib.hash import pbkdf2_sha256 as sha256


class UserModel(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)
    admin = db.Column(db.Boolean, unique=False, default=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_username(cls, username):
        print(db.engine.url)
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'id': x.id,
                'username': x.username
            }

        return {'users': list(map(lambda x: to_json(x), UserModel.query.all()))}

    @classmethod
    def delete_by_id(cls, id):
        cls.query.filter_by(id=id).delete()
        db.session.commit()
        return {'message': 'User with id {} have been deleted'.format(id)}, 200


    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)

    @staticmethod
    def verify_hash(password, hash):
        return sha256.verify(password, hash)

    @staticmethod
    def password_validation(password):

        config = ConfigModel.get_password_policy()

        def lower_error():
            return re.search(r"[a-z]", password)

        def symbol_error():
            return re.search(r"[!@$&]", password)

        def upcase_error():
            return re.search(r"[A-Z]", password)

        def digit_error():
            return re.search(r"\d", password)

        if len(password) != config.length:
            return False
        if config.lowercase_letters and lower_error() is None:
            return False
        if config.special_symbols and symbol_error() is None:
            return False
        if config.uppercase_letters and upcase_error() is None:
            return False
        if config.numbers and digit_error() is None:
            return False

        return True


    @classmethod
    def change_password(cls, id, password):
        if cls.password_validation(password):

            user = cls.query.filter_by(id=id).first()
            user.password = cls.generate_hash(password)

            db.session.commit()
            return {'message': 'For user with id {} password successfully changed'.format(id)}

        return {'message': 'Password policy not applying'}


class ConfigModel(db.Model):
    __tablename__ = 'config'

    id = db.Column(db.Integer, primary_key=True)

    length = db.Column(db.Integer, default=8)
    numbers = db.Column(db.Boolean, unique=False, default=False)
    uppercase_letters = db.Column(db.Boolean, unique=False, default=False)
    lowercase_letters = db.Column(db.Boolean, unique=False, default=False)
    special_symbols = db.Column(db.Boolean, unique=False, default=False)

    @classmethod
    def change_policy(cls, kwargs):
        cls.query.update(kwargs)
        db.session.commit()
        return {'message': 'Password policy successfully update'}

    @classmethod
    def get_password_policy(cls):
        return cls.query.first()


class RevokedTokenModel(db.Model):
    __tablename__ = 'revoked_tokens'
    id = db.Column(db.Integer, primary_key=True)
    jti = db.Column(db.String(120))

    def add(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def is_jti_blacklisted(cls, jti):
        query = cls.query.filter_by(jti=jti).first()
        return bool(query)
