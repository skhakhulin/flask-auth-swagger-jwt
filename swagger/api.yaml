
swagger: "2.0"

info:
  title: Authentication Microservice API
  version: 1.0.0

basePath: /api

tags:
- name: "Admins actions"
  description: "example"
- name: "Account"
  description: "example"
- name: "Auntification"
  description: "example"


schemes:
- "https"
- "http"

paths:

  '/accounts/login':
    post:
      tags: [Auntification]

      summary: Login
      description: ""

      operationId: api.accounts.login

      consumes:
      - "application/json"
      produces:
      - "application/json"

      parameters:
        - in: body
          name: account
          description: Account to login
          required: True
          schema:
            type: object
            properties:
              username:
                type: string
                description: Username for login
              password:
                type: string
                description: Password for login

      responses:
        '200':
          description: Login success
        '400':
          description: Invalid username/password

  '/accounts/logout':
    post:
      tags: [Auntification]

      summary: Logout
      description: ""

      operationId: api.accounts.logout

      parameters:
      - $ref: '#/parameters/token'

      responses:
        200:
          description: Logout success
        401:
          $ref: "#/responses/UnauthorizedError"
        403:
          $ref: "#/responses/TokenValidError"

  '/accounts':
    get:
      tags: [Account]

      summary: Fetch all accounts
      description: ""

      operationId: api.accounts.fetch_accounts

      responses:
        '200':
          description: Return accounts list
          schema:
            type: array
            items:
              properties:
                id:
                  type: integer
                  format: int64
                  description: Id
                username:
                  type: string
                  description: Username

    post:
      tags: [Admins actions]

      summary: Create Account
      description: ""

      operationId: api.accounts.create_account

      parameters:
        - $ref: '#/parameters/token'
        - in: body
          name: account
          description: Account to create
          required: True
          schema:
            type: object
            properties:
              username:
                type: string
                description: Login of account to create
              password:
                type: string
                description: Password of account to create

      responses:
        201:
          description: Successfully created Account
        401:
          $ref: "#/responses/UnauthorizedError"
        403:
          $ref: "#/responses/TokenValidError"
        405:
          $ref: "#/responses/MethodNotAllowed"

  '/accounts/{account_id}/password':
    put:
      tags: [Admins actions]

      summary: Change password
      description: ""

      operationId: api.accounts.change_password

      parameters:
        - $ref: '#/parameters/token'
        - $ref: '#/parameters/account_id'
        - in: body
          name: password
          description: change password
          required: True
          schema:
            type: object
            properties:
              password:
                type: string
                description: Password

      responses:
        200:
          description: Successfully change Password
        401:
          $ref: "#/responses/UnauthorizedError"
        403:
          $ref: "#/responses/TokenValidError"
        405:
          $ref: "#/responses/MethodNotAllowed"

  '/accounts/password/policy':
    post:
      tags: [Admins actions]

      summary: Change password policy
      description: ""

      operationId: api.accounts.change_policy

      parameters:
        - $ref: '#/parameters/token'
        - in: body
          name: password_policy
          description: Password settings
          required: True
          schema:
            type: object
            properties:
              length:
                type: number
                description: Minimal length

              numbers:
                type: boolean
                description: Must have number in password

              uppercase_letters:
                type: boolean
                description: Must have uppercase letters in password

              lowercase_letters:
                type: boolean
                description: Must have lowercase letters in password

              special_symbols:
                type: boolean
                description: Must have special symbols in password

      responses:
        200:
          description: Successfully change password policy
        401:
          $ref: "#/responses/UnauthorizedError"
        403:
          $ref: "#/responses/TokenValidError"
        405:
          $ref: "#/responses/MethodNotAllowed"

  '/accounts/{account_id}':
    delete:
      tags: [Admins actions]

      summary: Delete account
      description: ""

      operationId: api.accounts.delete_account

      parameters:
        - $ref: '#/parameters/token'
        - $ref: '#/parameters/account_id'

      responses:
        204:
          description: Account was deleted
        401:
          $ref: "#/responses/UnauthorizedError"
        403:
          $ref: "#/responses/TokenValidError"
        404:
          description: Account does not exist
        405:
          $ref: "#/responses/MethodNotAllowed"

parameters:
  account_id:
    in: path
    name: account_id
    type: string
    required: true
    pattern: "^[a-zA-Z0-9-]+$"
  token:
    in: header
    name: Authorization
    required: true
    type: string

responses:
  UnauthorizedError:
    description: Token is missing or invalid
    headers:
      WWW_Authenticate:
        type: string

  TokenValidError:
    description: Access token does not have the required scope
    headers:
      WWW_Authenticate:
        type: string

  MethodNotAllowed:
    description: Method Not Allowed
    headers:
      WWW_Authenticate:
        type: string