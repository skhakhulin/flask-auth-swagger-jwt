import requests
import unittest

from connexion.resolver import RestyResolver

from flask_testing import LiveServerTestCase

from app import db, connexion_app, jwt

from models import UserModel, ConfigModel, RevokedTokenModel

connexion_app.add_api("api.yaml", resolver=RestyResolver('api'))

@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return RevokedTokenModel.is_jti_blacklisted(jti)


class Tests(LiveServerTestCase):
    user = {
        'username': 'foo',
        'password': 'foo'
    }
    user_1 = {
        'username': 'bar',
        'password': 'bar'
    }
    root = {
        'username': 'root',
        'password': 'root'
    }

    def setUp(self):

        db.create_all()

        UserModel(
            username=self.root['username'],
            password=UserModel.generate_hash(self.root['password']),
            admin=True
        ).save_to_db()

        UserModel(
            username=self.user['username'],
            password=UserModel.generate_hash(self.user['password'])
        ).save_to_db()

        UserModel(
            username=self.user_1['username'],
            password=UserModel.generate_hash(self.user_1['password'])
        ).save_to_db()

        ConfigModel.change_policy({
            "length": 6,
            "lowercase_letters": False,
            "numbers": False,
            "special_symbols": False,
            "uppercase_letters": False
        })

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def create_app(self):
        app = connexion_app.app
        app.config.from_pyfile('settings/test.cfg')
        return app

    def get_root_access_token(self):
        return requests.post(
            f"{self.get_server_url()}/api/accounts/login",
            json=self.root
        ).json()['access_token']

    def get_user_access_token(self):
        return requests.post(
            f"{self.get_server_url()}/api/accounts/login",
            json=self.user
        ).json()['access_token']

    def test_get_accounts(self):
        response = requests.get(f"{self.get_server_url()}/api/accounts")
        self.assertEqual(response.status_code, 200)

        result = response.json()['users']
        self.assertEqual(len(result), 3)

    def test_add_new_user(self):
        root_token = self.get_root_access_token()
        user_token = self.get_user_access_token()

        response = requests.post(
            f"{self.get_server_url()}/api/accounts",
            json={
                'username': 'bar',
                'password': 'bar'
            },
            headers={
                'Authorization': user_token
            }
        )
        self.assertEqual(response.status_code, 405)

        response = requests.post(
            f"{self.get_server_url()}/api/accounts",
            json={
                'username': 'foo1',
                'password': 'foo1'
            },
            headers={
                'Authorization': root_token
            }
        )
        self.assertEqual(response.status_code, 201)

        response = requests.post(
            f"{self.get_server_url()}/api/accounts",
            json={
                'username': 'foo1',
                'password': 'foo1'
            },
            headers={
                'Authorization': root_token
            }
        )
        self.assertEqual(response.status_code, 400)

    def test_delete_user(self):

        root_token = self.get_root_access_token()
        user_token = self.get_user_access_token()

        response = requests.delete(
            f"{self.get_server_url()}/api/accounts/3",
            headers={
                'Authorization': user_token
            }
        )
        self.assertEqual(response.status_code, 405)

        response = requests.delete(
            f"{self.get_server_url()}/api/accounts/3",
            headers={
                'Authorization': root_token
            }
        )
        self.assertEqual(response.status_code, 200)

        response = requests.delete(
            f"{self.get_server_url()}/api/accounts/3",
            headers={
                'Authorization': root_token
            }
        )
        self.assertEqual(response.status_code, 404)

        response = requests.delete(
            f"{self.get_server_url()}/api/accounts/2",
            headers={
                'Authorization': user_token
            }
        )
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
