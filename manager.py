from connexion.resolver import RestyResolver

from app import connexion_app, db, jwt, manager
from models import UserModel, RevokedTokenModel, ConfigModel


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return RevokedTokenModel.is_jti_blacklisted(jti)


@manager.command
def runserver(*args, **kwargs):
    connexion_app.add_api("api.yaml", resolver=RestyResolver('api'))
    connexion_app.run(*args, **kwargs)


@manager.command
def init_db():
    db.create_all()

    if UserModel.query.filter_by(username='root').first() is None:
        UserModel(
            username='root',
            password=UserModel.generate_hash('root'),
            admin=True
        ).save_to_db()

    if len(UserModel.query.all()) == 0:
        ConfigModel.change_policy({
              "length": 6,
              "lowercase_letters": False,
              "numbers": False,
              "special_symbols": False,
              "uppercase_letters": False
        })




if __name__ == "__main__":
    manager.run()
